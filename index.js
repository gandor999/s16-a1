console.log("Hello");

let input = parseInt(prompt("Enter Input: "));

console.log("The number you provided is " + input);

for(let i = input; i > 0; --i){

	console.log(i);

	if(i <= 50){
		console.log("The current value is at or less than 50. Terminating the loop");
		break;
	}

	if(i % 10 === 0){
		console.log("Value is divisible by 10 therefore skipping the number");
		continue;
	}

	if(i % 5 === 0){
		console.log(i);
	}
}

let word = "supercalifragilisticexpialidocious";
let newWord = "";

for(let i = 0; i < word.length; ++i){

	if(
		word[i].toLowerCase() === 'a' ||
		word[i].toLowerCase() === 'e' ||
		word[i].toLowerCase() === 'i' ||
		word[i].toLowerCase() === 'o' ||
		word[i].toLowerCase() === 'u'
	){
		continue;
	}

	else{
		newWord += word[i];
	}

}
console.log(word);
console.log(newWord);